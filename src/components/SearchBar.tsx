import { ChangeEvent, FormEvent, PureComponent } from 'react'

import styles from './SearchBar.module.css'

export interface SearchBarProps {
  placeholder: string
  onSearch: (value: string) => void
}

export interface SearchBarState {
  value: string
}

class SearchBar extends PureComponent<SearchBarProps, SearchBarState> {
  state = {
    value: '',
  }

  handleInput = (event: ChangeEvent<HTMLInputElement>) => {
    this.setState({
      value: event.target.value,
    })
  }

  handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    this.props.onSearch(this.state.value)
    event.preventDefault()
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit} className={styles.SearchBar}>
        <input
          type="search"
          placeholder={this.props.placeholder}
          value={this.state.value}
          onChange={this.handleInput}
        />
        <button type="submit">Search</button>
      </form>
    )
  }
}

export default SearchBar
