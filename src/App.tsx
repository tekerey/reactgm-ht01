import { useCallback, useState } from 'react'

import Counter from './components/Counter'
import SearchBar from './components/SearchBar'
import GenreToggle from './components/GenreToggle'

import './styles.css'

export default function App() {
  const [search, setSearch] = useState('')

  const handleSearch = useCallback((value: string) => {
    setSearch(value)
  }, [])

  return (
    <div className="App">
      <h1>React Global Mentoring</h1>
      <h2>Home task #1</h2>
      <Counter />
      <SearchBar
        placeholder="What do you want to watch?"
        onSearch={handleSearch}
      />
      <p>Search: {search || 'Press search button or "Enter" button'}</p>
      <GenreToggle />
    </div>
  )
}
