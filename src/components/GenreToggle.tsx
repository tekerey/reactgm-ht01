import { createElement, FC, useState } from 'react'

import styles from './GenreToggle.module.css'

const genres = ['all', 'documentary', 'comedy', 'horror', 'crime']

export interface GenreToggleProps {
  onChange?: (value: string) => void
}

const GenreToggle: FC<GenreToggleProps> = ({ onChange }) => {
  const [selectedGenre, setSelectedGenre] = useState<string>(genres[0])

  const handleClick = (genre: string) => {
    setSelectedGenre(genre)
    onChange?.(genre)
  }

  return (
    <div className={styles.GenreToggle}>
      <div>
        {genres.map(genre =>
          // React.createElement
          createElement(
            'button',
            {
              key: genre,
              className: `${styles.GenreToggleButton} ${
                selectedGenre === genre && styles.active
              }`,
              onClick: () => handleClick(genre),
            },
            genre,
          ),
        )}
      </div>
    </div>
  )
}

export default GenreToggle
