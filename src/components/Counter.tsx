import { Component } from 'react'

import styles from './Counter.module.css'

export interface CounterState {
  counter: number
}

class Counter extends Component<{}, CounterState> {
  state = {
    counter: 0,
  }

  increment = () => {
    this.setState(state => ({
      counter: state.counter + 1,
    }))
  }

  decrement = () => {
    this.setState(state => ({
      counter: state.counter - 1,
    }))
  }

  render() {
    return (
      <div className={styles.Counter}>
        <button onClick={this.decrement}>-</button>
        <span>{this.state.counter}</span>
        <button onClick={this.increment}>+</button>
      </div>
    )
  }
}

export default Counter
